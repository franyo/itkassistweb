package hu.itkassist;

import com.google.gson.Gson;
import hu.itkassist.openephyra.OpenEphyra;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

@Path("/question")
public class ReceiveQuestion {

    @POST
    @Produces("text/plain")
    public String getClichedMessage(String question) {
        List<String[]> answerList = OpenEphyra.getInstance().ask(question);
        String answerListJson = new Gson().toJson(answerList);
        return answerListJson;
    }
}
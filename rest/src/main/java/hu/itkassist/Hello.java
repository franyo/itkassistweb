package hu.itkassist;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/helloassist")
public class Hello {

    @GET
    @Produces("text/plain")
    public String getClichedMessage() {
        return "Hello Assist!";
    }
}
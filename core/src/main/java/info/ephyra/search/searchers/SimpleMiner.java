package info.ephyra.search.searchers;

import java.util.List;

import info.ephyra.search.Result;

public class SimpleMiner extends KnowledgeMiner {

	public SimpleMiner(List<String> files) {
		super();
		this.files = files;
	}

	@Override
	protected int getMaxResultsTotal() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected int getMaxResultsPerQuery() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public KnowledgeMiner getCopy() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Result[] doSearch() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private List<String> files;
	
	private static final int MAX_RESULTS_TOTAL = 500;
	private static final int MAX_RESULTS_PERQUERY = 500;
	private static final int MAX_DOCS = 500;

}

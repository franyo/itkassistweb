package info.ephyra;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.ephyra.search.Result;

public class ITKAnswerCreator {
	
	private static ITKAnswerCreator ITKAC = null;
	private Map<String, String[]> possibleAnswers = null;
	
	private ITKAnswerCreator() {
		//TODO log
		possibleAnswers = new HashMap<String, String[]>();
		String path = System.getProperty("user.dir") + "/res/knowledgebase/questions/questions.txt";
		BufferedReader reader = null;
		try {
			
			reader = new BufferedReader(new FileReader(path));
		    String line = null;
		    while ((line = reader.readLine()) != null) {
		        String[] parts = line.split(";");
		        if(parts.length >= 3) {
		        	possibleAnswers.put(parts[0], new String[]{parts[1], parts[2]});
		        }
		    }
		} catch (IOException x) {
		    System.err.format("IOException: %s%n", x);
		} finally {
			if(reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static ITKAnswerCreator getITKAnswerCreator() {
		if(ITKAC == null) {
			ITKAC = new ITKAnswerCreator();
		}
		return ITKAC;
	}
	
	public List<String[]> createAnswers(Result[] results) {
		//TODO log
		ArrayList<String[]> qa = new ArrayList<String[]>();
		ArrayList<Result> uniqResults = new ArrayList<>();
		for (Result res : results) {
			if (!uniqResults.contains(res)) {
				uniqResults.add(res);
			}
		}

		for (Result res : uniqResults) {
			String doc = res.getDocID();
			int index = doc.lastIndexOf("/");
			if(index < 0)
				index = doc.lastIndexOf("\\");
			String filename = doc.substring(index + 1);

			String[] answer = possibleAnswers.get(filename);
			qa.add(answer);
		}

//
//		ArrayList<String> tmp = new ArrayList<String>();
//
//		Map<String, Integer> counter = new HashMap<String, Integer>();
//
//		//System.out.println(results.length);
//		for (int i = 0; i < results.length; i++) {
//			String doc = results[i].getDocID();
//			int index = doc.lastIndexOf("/");
//			if(index < 0)
//				index = doc.lastIndexOf("\\");
//			String filename = doc.substring(index + 1);
//			//System.out.println(filename);
//
//			if(counter.containsKey(filename)) {
//				counter.put(filename, counter.get(filename) + 1);
//				continue;
//			}
//			counter.put(filename, 1);
//			tmp.add(filename);
//		}
//		/*String[] tmpA = tmp.toArray(new String[tmp.size()]);
//		System.out.println(tmpA.length);*/
//		String[] sortedArray = quickSort(counter, tmp.toArray(new String[tmp.size()]), 0, tmp.size() - 1);
//
//		for(int i = 0; i < sortedArray.length; i++) {
//			//System.out.println(sortedArray[i]);
//			String[] answer = possibleAnswers.get(sortedArray[i]);
//			if(answer != null) {
//				qa.add(answer);
//			}
//		}

		return qa;
	}
	
//	private String[] quickSort(Map<String, Integer> map, String[] array, int lowerIndex, int higherIndex) {
//
//		if(array.length < 2)
//			return array;
//
//        int i = lowerIndex;
//        int j = higherIndex;
//        int pivot = map.get(array[lowerIndex+(higherIndex-lowerIndex)/2]);
//
//        while (i <= j) {
//            while (map.get(array[i]) > pivot) {
//                i++;
//            }
//            while (map.get(array[j]) < pivot) {
//                j--;
//            }
//            if (i <= j) {
//            	exchangeValues(array, i, j);
//                i++;
//                j--;
//            }
//        }
//        if (lowerIndex < j)
//            quickSort(map, array, lowerIndex, j);
//        if (i < higherIndex)
//            quickSort(map, array, i, higherIndex);
//
//        return array;
//    }
 
    private void exchangeValues(String[] array, int i, int j) {
        String temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

}

package hu.itkassist.openephyra;

import hu.itkassist.AnswerMachine;
import info.ephyra.io.MsgPrinter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class OpenEphyra implements AnswerMachine {

    private Integer questionCount;
    private info.ephyra.OpenEphyra ephyra;

    private OpenEphyra() {
        questionCount = 0;
        ephyra = new info.ephyra.OpenEphyra(System.getProperty("user.dir"));
    }

    private static OpenEphyra instance = null;

    public static OpenEphyra getInstance() {
        if (instance == null) {
            instance = new OpenEphyra();
        }
        return instance;
    }

    public List<String[]> ask(String question) {
    	if(question.length() > 0)
    		return ephyra.ask(question);
    	return null;
    }

    private static String readLine()
    {
        try
        {
            return new java.io.BufferedReader(new java.io.InputStreamReader(
                System.in)).readLine();
        }
        catch (java.io.IOException e)
        {
            return new String("");
        }
    }

    public static void main(String[] args) {
        OpenEphyra.getInstance();

        while (true) {
            MsgPrinter.printQuestionPrompt();
            String question = readLine().trim();

            if (question.equals(""))
                continue;

            if (question.equals("exit") || question.equals("quit"))
                break;

            List<String[]> answerList = OpenEphyra.getInstance().ask(question);

            if(answerList.isEmpty()) {
                System.out.println("Sorry, I don't know. :(");
            }

            for (String[] answer : answerList) {
                System.out.println(answer[0]);
                System.out.println(answer[1]);
            }
        }

        System.exit(0);


//    	List<String[]> answerList = OpenEphyra.getInstance().ask("How can I implement a fire detection system?");
//    	//List<String[]> answerList = OpenEphyra.getInstance().ask("How can I adopt a child?");
//
//    	if(answerList.isEmpty())
//    		System.out.println("Sorry, I don't know. :(");
//
//        for (String[] answer : answerList) {
//            System.out.println(answer[0]);
//            System.out.println(answer[1]);
//        }
    }
}
package hu.itkassist;

import java.util.List;

public interface AnswerMachine {

    List<String[]> ask(String question);
}
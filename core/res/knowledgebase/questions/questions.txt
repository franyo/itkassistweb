Animal.txt;Would you like to know something about animals and pests?;https://www.gov.uk/browse/housing-local-services/noise-neighbours
Child.txt;Are you interested in child adoption?;https://www.gov.uk/child-adoption
DrivingLicence.txt;Do you want to renew your driving licence?;https://www.gov.uk/renew-driving-licence
Education-qualification.txt;Are you interested in education?;https://www.gov.uk/browse/education
EmployingPeople.txt;Do you want to know something about employing people?;https://www.gov.uk/browse/employing-people
Fishing.txt;Are you interested in fishing and hunting?;https://www.gov.uk/browse/environment-countryside/fishing-hunting
Law&Rights.txt;Would you like to get information about law and rights?;https://www.gov.uk/browse/justice/rights
Passport.txt;Would you get some help with passport?;https://www.gov.uk/browse/abroad/passports
TravelAbroad.txt;Would you like to get information about travel?;https://www.gov.uk/browse/abroad/travel-abroad
Voting.txt;Would you like to get information about voting?;https://www.gov.uk/browse/citizenship/voting
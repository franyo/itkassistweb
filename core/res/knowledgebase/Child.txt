﻿1. Overview
To be adopted, a child must:

be under the age of 18 when the adoption application is made
not be (or have never been) married or in a civil partnership
The child’s birth parents
Both birth parents normally have to agree (consent) to the adoption, unless:

they can’t be found
they’re incapable of giving consent, eg due to a mental disability
the child would be put at risk if they weren’t adopted
Who can adopt a child
You may be able to adopt a child if you’re aged 21 or over
(there’s no upper age limit) and either:

single
married
in a civil partnership
an unmarried couple (same sex and opposite sex)
the partner of the child’s parent
There are different rules for private adoptions and adoptions
of looked-after children.

Living in the UK
You don’t have to be a British citizen to adopt a child, but:

you (or your partner, if you’re a couple) must have a fixed and permanent
home in the UK, Channel Islands or the Isle of Man
you (and your partner, if you’re a couple) must have lived in the UK for at
least 1 year before you begin the application process
